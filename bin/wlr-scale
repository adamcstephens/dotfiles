#!/usr/bin/env python3

from sys import argv, exit
from subprocess import run
from json import loads

current = loads(
    run(["wlr-randr", "--json"], capture_output=True).stdout.decode("utf-8")
)

if len(argv) < 2 or not argv[1] in ["up", "down", "reset"]:
    print("usage: wlr-scale (up|down|reset)")
    exit(1)

if argv[1] == "reset":
    run(["systemctl", "--user", "restart", "kanshi"])
    exit(0)

for display in current:
    if not display["enabled"]:
        continue

    scale_factor = 0.05
    if argv[1] == "down":
        scale_factor *= -1

    run(["systemctl", "--user", "stop", "kanshi"])

    print(display["name"])
    prior_scale = display["scale"]
    new_scale = prior_scale * scale_factor + prior_scale

    run(["wlr-randr", "--json", "--output", display["name"], "--scale", str(new_scale)])
