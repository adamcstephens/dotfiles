--
-- set the color and enable auto dark
--
vim.cmd.colorscheme("moonfly")
if vim.loop.os_uname().sysname ~= "Linux" or os.getenv("XDG_CURRENT_DESKTOP") ~= nil then
  require("auto-dark-mode").setup({
    update_interval = 5000,
    set_dark_mode = function()
      vim.opt.background = "dark"
      vim.cmd.colorscheme("moonfly")
    end,
    set_light_mode = function()
      vim.opt.background = "light"
      vim.cmd.colorscheme("github_light_colorblind")
    end,
  })
end

require("github-theme").setup({
  options = {
    styles = {
      comments = "italic",
    },
  },
})

require("lualine").setup({
  options = { theme = "auto" },
  sections = {
    lualine_c = { { "filename", path = 1 } },
  },
})

require("modus-themes").setup({
  dim_inactive = false,
  on_highlights = function(highlights, colors)
    -- default is too much blue
    highlights.Identifier = { fg = colors.fg_main }

    highlights.NeogitBranch = { fg = colors.blue }
    highlights.NeogitRemote = { fg = colors.magenta }
    highlights.NeogitSectionHeader = { fg = colors.fg_main }
    highlights.NeogitChangeModified = { fg = colors.blue }

    highlights.NeogitHunkHeader = { bg = colors.bg_active }
    highlights.NeogitDiffContext = { fg = colors.fg_dim }
    highlights.NeogitDiffAdd = { bg = colors.bg_added, fg = colors.fg_added }
    highlights.NeogitDiffDelete = { bg = colors.bg_removed, fg = colors.fg_removed }

    highlights.NeogitHunkHeaderHighlight = { bg = colors.bg_active, bold = true }
    highlights.NeogitDiffContextHightlight = { bg = colors.bg_dim, fg = colors.fg }
    highlights.NeogitDiffAddHighlight = { bg = colors.bg_added, fg = colors.fg_added }
    highlights.NeogitDiffDeleteHighlight = { bg = colors.bg_removed, fg = colors.fg_removed }

    highlights.NeogitFilePath = { fg = colors.green_faint }
    highlights.NeogitCommitViewHeader = { fg = colors.blue }

    highlights.IlluminatedWordRead = { underline = true }
    highlights.IlluminatedWordWrite = { underline = true }
    highlights.IlluminatedWordText = { underline = true }

    highlights.DiagnosticWarn = { bg = colors.bg_active }

    -- remove background from floating windows
    highlights.NormalFloat = { fg = "none", bg = "none" }
    highlights.FloatBorder = { fg = "none", bg = "none" }
  end,
})

require("nvim-highlight-colors").setup({
  render = "first_column",
})

require("nvim-highlight-colors").turnOff()

require("rainbow-delimiters.setup").setup({})

-- show matches of hovered word
require("illuminate").configure({
  providers = {
    "lsp",
    "treesitter",
  },
  case_insensitive_regex = false,
  delay = 100,
  large_file_cutoff = nil,
  large_file_overrides = nil,
  min_count_to_highlight = 1,
  under_cursor = true,
})
