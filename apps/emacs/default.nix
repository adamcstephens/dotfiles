{
  config,
  inputs,
  lib,
  pkgs,
  ...
}:
let
  cfg = config.dotfiles.apps.emacs;

  aspell = pkgs.aspellWithDicts (
    dicts: with dicts; [
      en
      en-computers
    ]
  );

  extraBins =
    [
      pkgs.biome
      pkgs.coreutils
      pkgs.curl
      pkgs.diffutils
      pkgs.fd
      pkgs.fzf
      pkgs.git
      pkgs.python3Minimal
      pkgs.ripgrep
      pkgs.shellcheck
      pkgs.shfmt
      pkgs.sqlite
    ]
    ++ (lib.optionals cfg.full [
      aspell
      pkgs.ghostscript
      pkgs.groff
      pkgs.graphicsmagick
      pkgs.mermaid-cli
      pkgs.multimarkdown
      pkgs.nodejs
      pkgs.pandoc
      pkgs.python3Packages.weasyprint
    ]);

  emacsPatched = cfg.package.overrideAttrs (prev: {
    patches =
      lib.optionals pkgs.stdenv.isLinux [ ./silence-pgtk-xorg-warning.patch ]
      ++ lib.optionals pkgs.stdenv.isDarwin [
        # "${inputs.emacs-plus}/patches/emacs-28/fix-window-role.patch"
        # "${inputs.emacs-plus}/patches/emacs-28/no-frame-refocus-cocoa.patch"
        # "${inputs.emacs-plus}/patches/emacs-29/poll.patch"
        # # "${inputs.emacs-plus}/patches/emacs-30/round-undecorated-frame.patch"
        # "${inputs.emacs-plus}/patches/emacs-28/system-appearance.patch"
      ]
      ++ prev.patches;
  });

  selectedPackage = if cfg.patchForGui then emacsPatched else cfg.package;

  revealjs = pkgs.callPackage ./revealjs.nix { };

  pins-packages = import ./npins-packages;
  npinsPackages =
    epkgs:
    lib.mapAttrsToList (
      name: src:
      (epkgs.trivialBuild {
        pname = name;
        inherit src;
        version = "unstable-${builtins.substring 0 8 src.revision}";
      })
    ) pins-packages;

  emacsPackages =
    epkgs:

    let
      env = ''
        (setq exec-path (append exec-path '( ${
          lib.concatMapStringsSep " " (x: ''"${x}/bin"'') extraBins
        } )))

        (defun dot/font-mono ()
          "${config.dotfiles.gui.font.mono}")

        (defun dot/font-variable ()
          "${config.dotfiles.gui.font.variable}")

        ${lib.optionalString cfg.full ''(setq org-re-reveal-root "${revealjs.outPath}")''}

        (add-to-list 'custom-theme-load-path "${pins-packages.doom-moonfly-theme}")

        (provide 'dotemacs-nix-env)
      '';
    in
    (npinsPackages epkgs)
    ++ [
      (epkgs.trivialBuild {
        pname = "dotemacs-nix-env";
        src = pkgs.writeText "dotemacs-nix-env.el" env;
        version = "0.1.0";
      })
      epkgs.treesit-grammars.with-all-grammars
    ]
    ++ (with epkgs.melpaStablePackages; [ org-re-reveal ])
    ++ (with epkgs.melpaPackages; [
      agenix
      all-the-icons
      apheleia
      auto-dark
      avy
      bbww
      cape
      chatgpt-shell
      cider
      clipetty
      consult
      corfu
      dash
      diff-ansi
      diff-hl
      direnv
      dirvish
      doom-modeline
      doom-themes
      editorconfig
      eldoc-box
      elisp-autofmt
      elixir-ts-mode
      embark
      embark-consult
      evil
      evil-collection
      evil-commentary
      evil-org
      evil-terminal-cursor-changer
      expand-region
      fish-mode
      flyspell-correct
      gcmh
      git-auto-commit-mode
      golden-ratio
      haskell-mode
      hide-mode-line
      just-mode
      kkp
      ligature
      lispy
      magit
      marginalia
      markdown-mode
      modus-themes
      move-dup
      multi-vterm
      mwim
      nim-mode
      nix-ts-mode
      nushell-ts-mode
      ob-mermaid
      olivetti
      orderless
      org-appear
      org-autolist
      org-download
      org-present
      org-superstar
      ox-pandoc
      persistent-scratch
      project-rootfile
      rainbow-delimiters
      ron-mode
      run-command
      ssh-config-mode
      terraform-mode
      transpose-frame
      treesit-auto
      undo-fu
      undo-fu-session
      vertico
      vterm
      wgrep
      which-key
      whole-line-or-region
      yasnippet
      yasnippet-snippets
      yuck-mode
    ])
    ++ (with epkgs.elpaPackages; [
      org
      rainbow-mode
      substitute
      vundo
    ])
    ++ (with epkgs.nongnuPackages; [ eat ]);

  emacsPackage = (pkgs.emacsPackagesFor selectedPackage).emacsWithPackages emacsPackages;

  package =
    let
      path = "${lib.makeBinPath extraBins}:${config.home.homeDirectory}/.dotfiles/bin";
      args = "${lib.optionalString cfg.full "--set FONTCONFIG_FILE ${config.dotfiles.gui.font.fontconfig}"} --prefix PATH : ${path}";
    in
    pkgs.symlinkJoin {
      name = "dotemacs";

      paths = [ emacsPackage ];

      nativeBuildInputs = [ pkgs.makeWrapper ];

      postBuild = ''
        wrapProgram "$out/bin/emacs" ${args}
        ${lib.optionalString pkgs.stdenv.isDarwin ''
          wrapProgram "$out/Applications/Emacs.app/Contents/MacOS/Emacs" ${args}
        ''}
      '';

      inherit (emacsPackage) meta;
    };
in
{
  options = {
    dotfiles.apps.emacs = {
      enable = lib.mkEnableOption "emacs";

      package = lib.mkOption {
        type = lib.types.package;
        default = if pkgs.stdenv.isLinux then pkgs.emacs29-pgtk else pkgs.emacs29;
      };

      patchForGui = lib.mkEnableOption "patch emacs as if for a gui install";

      full = lib.mkEnableOption "install the full set of tools, as if a workstation";
    };
  };
  config = lib.mkIf cfg.enable {
    programs.doom-emacs = {
      enable = true;
      doomDir = ./doom.d;
      doomLocalDir = "${config.home.homeDirectory}/.dotfiles/apps/emacs/doom.d";
      # extraPackages = epkgs: [ epkgs.modus-themes ];
    };
    # home.packages = [ package ];
    #
    # xdg.configFile =
    #   if config.dotfiles.nixosManaged then
    #     {
    #       "emacs/elisp".source = ./elisp;
    #       "emacs/snippets".source = ./snippets;
    #       "emacs/custom.el".source = ./custom.el;
    #       "emacs/early-init.el".source = ./early-init.el;
    #       "emacs/init.el".source = ./init.el;
    #       "emacs/straight/versions/default.el".source = ./straight/versions/default.el;
    #     }
    #   else
    #     {
    #       "emacs".source = config.lib.file.mkOutOfStoreSymlink "${config.home.homeDirectory}/.dotfiles/apps/emacs";
    #     };
    #
    # services = lib.mkIf pkgs.stdenv.isLinux {
    #   emacs = {
    #     enable = true;
    #     extraOptions = [
    #       "--init-directory"
    #       "${config.home.homeDirectory}/.config/emacs"
    #     ];
    #     package = package;
    #     socketActivation.enable = config.dotfiles.nixosManaged;
    #   };
    # };
    #
    # systemd = lib.mkIf pkgs.stdenv.isLinux {
    #   user.services.emacs.Service = {
    #     TimeoutSec = 900;
    #   };
    # };
    #
    # launchd = lib.mkIf pkgs.stdenv.isDarwin {
    #   agents.emacs = {
    #     enable = true;
    #     config = {
    #       KeepAlive = true;
    #       ProgramArguments = [
    #         "${config.home.homeDirectory}/.nix-profile/bin/fish"
    #         "-l"
    #         "-c"
    #         "${package}/bin/emacs --fg-daemon --init-directory ${config.home.homeDirectory}/.config/emacs"
    #       ];
    #       RunAtLoad = true;
    #       StandardErrorPath = "${config.home.homeDirectory}/.config/emacs/launchd.log";
    #       StandardOutPath = "${config.home.homeDirectory}/.config/emacs/launchd.log";
    #       WatchPaths = [ "${config.home.homeDirectory}/.nix-profile/bin/emacs" ];
    #     };
    #   };
    # };
  };
}
