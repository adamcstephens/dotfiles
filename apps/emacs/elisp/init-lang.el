(use-package go-ts-mode :mode "\\.go\\'")
(use-package nix-ts-mode :mode "\\.nix\\'")

(provide 'init-lang)
