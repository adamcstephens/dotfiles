{
  config,
  inputs,
  lib,
  pkgs,
  ...
}:
let
  cfg = config.dotfiles.apps.vscode;

  # wrap vscode so we can trick it to use proper secrets storage
  vscode = pkgs.symlinkJoin {
    inherit (pkgs.vscode)
      meta
      name
      pname
      version
      ;

    paths = [ pkgs.vscode ];
    nativeBuildInputs = [ pkgs.makeWrapper ];
    postBuild = ''
      wrapProgram $out/bin/code --set XDG_CURRENT_DESKTOP GNOME
    '';
  };
  prefix = if pkgs.stdenv.isDarwin then "Library/Application Support" else ".config";
in
{
  options.dotfiles.apps.vscode.enable = lib.mkEnableOption "vscode";

  config = lib.mkIf cfg.enable {
    assertions = [
      {
        assertion = !config.dotfiles.apps.vscodium.enable;
        message = "vscodium and vscode are mutually exclusive";
      }
    ];

    home.file."${prefix}/Code/User/keybindings.json".source = config.lib.file.mkOutOfStoreSymlink "${config.home.homeDirectory}/.dotfiles/apps/vscodium/keybindings.json";
    home.file."${prefix}/Code/User/settings.json".source = config.lib.file.mkOutOfStoreSymlink "${config.home.homeDirectory}/.dotfiles/apps/vscodium/settings.json";

    # they say you shouldn't modify the system in this phase, but... 🤷‍♂️
    home.activation.own-vscode-snippets = lib.hm.dag.entryBefore [ "checkLinkTargets" ] ''
      if [ ! -h "${config.home.homeDirectory}/${prefix}/Code/User/snippets" ]; then
        rm -rfv "${config.home.homeDirectory}/${prefix}/Code/User/snippets"
      fi
    '';
    home.file."${prefix}/Code/User/snippets".source = config.lib.file.mkOutOfStoreSymlink "${config.home.homeDirectory}/.dotfiles/apps/vscodium/snippets";

    programs.vscode = lib.mkIf pkgs.stdenv.isLinux {
      enable = true;
      package = vscode;

      extensions = with inputs.nix-vscode-extensions.extensions.${pkgs.system}.vscode-marketplace; [
        github.copilot
        ms-vsliveshare.vsliveshare
      ];
    };
  };
}
